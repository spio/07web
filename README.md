# Temat
Testowanie API.

# Cel
Celem zajęć jest przedstawienie wybranych elementów testowania API, z wykorzystaniem aplikacji web napisanych w środowisku Spring Boot.

# Technologie
* Java 8,
* JUnit 5.x - https://junit.org/junit5/docs/current/user-guide/
* AssertJ - http://joel-costigliola.github.io/assertj/
* RestAssured - http://rest-assured.io/, https://github.com/rest-assured/rest-assured/wiki/Usage#spring-mock-mvc-module

## Zadania

### Zadanie 1
Należy wykonać eksplorację API udostępnionego przez aplikację translatora z wykorzystaniem narzędzi typu POSTMAN oraz curl. Aplikacja udostępnia endpointy zgodne z opisem kontrolera: https://gitlab.com/spio-sources/translator/blob/master/jtranslator/rest/src/main/java/pl/poznan/put/spio/controller/GaDeRyPoLuKiController.java

Przykładowe żądanie:
```bash
pawel.martenka@DESKTOP-KRAEH13 MINGW64 ~
$ curl localhost:8080/gaderypoluki/kot
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    37    0    37    0     0    237      0 --:--:-- --:--:-- --:--:--   237{"origin":"kot","translated":"ipt"}
```

Projekt: https://gitlab.com/spio-sources/translator/tree/master/jtranslator

Dodatkowo, można dokonać eksploracji usługi SOAP/WebService, która jest wystawiona w sąsiednim module: https://gitlab.com/spio-sources/translator/blob/master/jtranslator/ws/src/main/java/pl/poznan/put/spio/ws/GaDeRyPoLuKiEndpoint.java

Przykładowe żądanie:
```bash
pawel@LAPTOP-SFLO27LV MINGW64 /c/dev/repos/spio-sources/translator/jtranslator (master)
$ curl --header "content-type: text/xml" -d @request.xml http://localhost:8080/services/gaderypoluki
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   527  100   260  100   267  11818  12136 --:--:-- --:--:-- --:--:-- 23954<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:translateResponse xmlns:ns2="http://pl.poznan.put.spio/"><return><origin>kot</origin><translated>ipt</translated></return></ns2:translateResponse></soap:Body></soap:Envelope>

```


### Zadanie 2

#### Wprowadzenie
Biblioteka RestAssured jest jednym z rozwiązań do testowania aplikacji od strony API. Korzystając z konfiguracji szykowanej bezpośrednio w kodzie oraz składni sprawdzeń w stylu AssertJ można sprawnie i czytelnie napisać testy żądań dla poszczególnych endpointów.

Przykład: shouldTranslate, shouldGetBadRequestOnInternalException

#### Treść
Należy napisać testy kolejnych endpointów aplikacji, z uwzględnieniem zwracanych kodów błędów. Aktualnie aplikacja posiada następujące endpointy:
* /gaderypoluki/{origin}
* /gaderypoluki/{origin}/ignorecase
* /gaderypoluki/{origin}/with/{key}
* /gaderypoluki/{origin}/with/{key}/ignorecase

Gdzie origin to wiadomość do przetłumaczenia, a key to klucz do translacji. Wszystkie wyjątki, które pojawią się w kontrolerze zostaną przechwycone i system zwróć kod BAD_REQUEST (400). Akceptowana metoda to GET. Wynikiem żądań jest JSON postaci:
* {"origin":"kot","translated":"apy","key":"politykaremu"}
* {"origin":"kot","translated":"ipt"}

Parametr key w odpowiedzi pojawia się tylko, gdy przyjdzie w żądaniu.

Projekt: https://gitlab.com/spio-sources/translator/tree/master/jtranslator
Testy RestAssured: https://gitlab.com/spio-sources/translator/tree/master/jtranslator/rest/src/test/java/pl/poznan/put/spio/web
